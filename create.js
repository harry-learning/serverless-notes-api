/*
 * @Author: Harry Tang - harry@powerkernel.com
 * @Date: 2020-01-18 17:34:47
 * @Last Modified by: Harry Tang - harry@powerkernel.com
 * @Last Modified time: 2020-01-18 18:10:39
 */

import uuid from "uuid";
import * as dynamoDbLib from "./libs/dynamodb-lib";
import { success, failure } from "./libs/response-lib";

export const main = async (event, context, cb) => {
  const data = JSON.parse(event.body);

  const params = {
    TableName: process.env.tableName,
    Item: {
      noteId: uuid.v1(),
      userId: event.requestContext.identity.cognitoIdentityId,
      content: data.content,
      attachment: data.attachment,
      createAt: Date.now()
    }
  };

  try {
    await dynamoDbLib.call("put", params);
    return success(params.Item);
  } catch (error) {
    return failure({ status: false });
  }
};
