/*
 * @Author: Harry Tang - harry@powerkernel.com
 * @Date: 2020-01-18 21:32:11
 * @Last Modified by: Harry Tang - harry@powerkernel.com
 * @Last Modified time: 2020-01-18 21:34:02
 */
export const calculateCost = storgae => {
  const rate = storgae <= 10 ? 4 : storgae <= 100 ? 2 : 1;
  return storgae * rate * 100;
};
