/*
 * @Author: Harry Tang - harry@powerkernel.com
 * @Date: 2020-01-18 18:04:05
 * @Last Modified by:   Harry Tang - harry@powerkernel.com
 * @Last Modified time: 2020-01-18 18:04:05
 */
import AWS from "aws-sdk";

export function call(action, params) {
  const dynamoDb = new AWS.DynamoDB.DocumentClient();
  return dynamoDb[action](params).promise();
}
