/*
 * @Author: Harry Tang - harry@powerkernel.com
 * @Date: 2020-01-18 17:57:12
 * @Last Modified by: Harry Tang - harry@powerkernel.com
 * @Last Modified time: 2020-01-18 18:09:16
 */

export function success(body) {
  return buildRes(200, body);
}

export function failure(body) {
  return buildRes(500, body);
}

function buildRes(code, body) {
  return {
    statusCode: code,
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true
    },
    body: JSON.stringify(body)
  };
}
