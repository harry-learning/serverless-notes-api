/*
 * @Author: Harry Tang - harry@powerkernel.com
 * @Date: 2020-01-18 19:55:28
 * @Last Modified by: Harry Tang - harry@powerkernel.com
 * @Last Modified time: 2020-01-18 21:39:58
 */

import stripePackage from "stripe";
import { calculateCost } from "./libs/billing-lib.js";
import { success, failure } from "./libs/response-lib";

export const main = async (event, context) => {
  const { storage, source } = JSON.parse(event.body);
  const amount = calculateCost(storage);
  const description = "Notes charge";

  const stripe = stripePackage(process.env.stripeSecretKey);

  try {
    await stripe.charges.create({
      source,
      amount,
      description,
      currency: "usd"
    });
    return success({ status: true });
  } catch (error) {
    return failure({ status: false, error: error });
  }
};
